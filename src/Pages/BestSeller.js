import React, { useEffect, useState } from 'react';
import axios from 'axios';
import './itemlist.css';
import { Card, CardBody, CardTitle, CardText, CardImg, Container, Row, Col } from 'reactstrap';

const BestSeller = () => {
    const [items, setItems] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:3201/items')
            .then(response => {
                setItems(response.data);
            })
            .catch(error => {
                console.error('There was an error fetching the items!', error);
            });
    }, []);

    return (
        <Container>
            <Row>
                {items.map(item => (
                    item.rating > '4.6  ' && (
                        <Col xs="12" sm="6" md="4" lg="3" xl="2" key={item._id} className="mb-4 d-flex">
                            <Card className="item-card">
                                {item.discount > 0 && <div className="discount-tag">{item.discount}%</div>}
                                <CardImg top width="100%" src={item.url} alt={item.title} className="item-image"/>
                                <CardBody className="d-flex flex-column">
                                    <CardTitle tag="h6" className="font-weight-bold">{item.title}</CardTitle>
                                    <CardText tag="small" className="mt-auto">{item.author}</CardText>
                                    <CardText className="text-muted text-decoration-line-through">
                                        ₹{item.price}
                                    </CardText>
                                    <CardText className="price">
                                        ₹{item.discount > 0 
                                            ? Math.floor(item.price * (1 - item.discount / 100)) 
                                            : item.price}
                                    </CardText>
        
                                    
                                </CardBody>
                            </Card>
                        </Col>
                    )
                ))}
            </Row>
        </Container>
    );
};

export default BestSeller;
