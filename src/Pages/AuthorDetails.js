import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import './AuthorDetail.css';

const AuthorDetail = () => {
  const { authorId } = useParams();
  const [authorData, setAuthorData] = useState(null);

  useEffect(() => {
    axios.get(`http://localhost:1200/items/${authorId}`)
      .then(response => {
        setAuthorData(response.data);
      })
      .catch(error => {
        console.error("There was an error fetching the author data!", error);
      });
  }, [authorId]);

  if (!authorData) {
    return <div>Loading...</div>;
  }

  return (
    <div className="author-detail">
      <img src={authorData.url} alt={authorData.author} className="author-image" />
      <h2>{authorData.author}</h2>
      <p><strong>Title:</strong> {authorData.title}</p>
      <p><strong>Published Year:</strong> {authorData.pbyear}</p>
      <p><strong>Description:</strong> {authorData.description}</p>
      <p><strong>Genre:</strong> {authorData.genre.main} - {authorData.genre.sub}</p>
      <p><strong>Price:</strong> ₹{authorData.price}</p>
      <p><strong>Discount:</strong> {authorData.discount}%</p>
      <p><strong>Language:</strong> {authorData.language}</p>
      <p><strong>Quantity:</strong> {authorData.quantity}</p>
      <p><strong>Publisher:</strong> {authorData.publisher}</p>
      <p><strong>Rating:</strong> {authorData.rating}</p>
    </div>
  );
};

export default AuthorDetail;
