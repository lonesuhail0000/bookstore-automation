import React, { useState } from 'react';
import axios from 'axios';

const url = 'http://localhost:3201/details';

const BookDetails = () => {
  const initialValues = { 
    ISBN: "", title: "", author: "", pbyear: "", description: "", price: "", url: "", 
    main: "", sub: "", discount: "", language: "", quantity: "", publisher: "", rating: ""
  };
  
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isRegistered, setIsRegistered] = useState(false);
  const [registrationAttempted, setRegistrationAttempted] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const validate = (values) => {
    const errors = {};
    if (!values.ISBN || isNaN(values.ISBN)) errors.ISBN = "Valid ISBN is required";
    if (!values.title) errors.title = "Title is required";
    if (!values.author) errors.author = "Author is required";
    if (!values.pbyear) errors.pbyear = "Publication year is required";
    if (!values.description) errors.description = "Description is required";
    if (!values.price || isNaN(values.price)) errors.price = "Valid price is required";
    if (!values.url) errors.url = "URL is required";
    if (!/^https?:\/\/.*\.(?:png|jpg|jpeg)$/i.test(values.url)) errors.url = "Valid image URL is required";
    if (!values.main) errors.main = "Main genre is required";
    if (!values.sub) errors.sub = "Sub-genre is required";
    if (!values.discount) errors.discount = "Discount is required";
    if (!values.language) errors.language = "Language is required";
    if (!values.quantity || isNaN(values.quantity)) errors.quantity = "Valid quantity is required";
    if (!values.publisher) errors.publisher = "Publisher is required";
    if (!values.rating) errors.rating = "Rating is required";
    // if(values.password !== values.confrmPassword) errors.confrmPassword="Password does not match" 
    return errors;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const errors = validate(formValues);
    setFormErrors(errors);

    if (Object.keys(errors).length === 0) {
      try {
        await axios.post(url, {
          ISBN: parseInt(formValues.ISBN),
          title: formValues.title,
          author: formValues.author,
          pbyear: formValues.pbyear,
          description: formValues.description,
          price: parseInt(formValues.price),
          url: formValues.url,
          genre: {
            main: formValues.main,
            sub: formValues.sub
          },
          discount: formValues.discount,
          language: formValues.language,
          quantity: parseInt(formValues.quantity),
          publisher: formValues.publisher,
          rating: formValues.rating
        });
        setIsRegistered(true);
        setErrorMessage("");
        handleReset();
      } catch (error) {
        setIsRegistered(false);
        setErrorMessage(error.response?.data?.message || "An error occurred during registration");
      }
      setRegistrationAttempted(true);
    }
  };

  const handleReset = () => {
    setFormValues(initialValues);
    setFormErrors({});
  };

  return (
    <div className='bigContainer'>
      <div className='Regcontainer'>
        <h3>Form For Book Details</h3>
        <form onSubmit={handleSubmit}>
          <div className='field'>
            <label>ISBN</label>
            <input
              type="text"
              name='ISBN'
              value={formValues.ISBN}
              onChange={handleChange}
            />
            {formErrors.ISBN && <span className="error">{formErrors.ISBN}</span>}
          </div>
          <div className='field'>
            <label>Book Title</label>
            <input
              type="text"
              name='title'
              value={formValues.title}
              onChange={handleChange}
            />
            {formErrors.title && <span className="error">{formErrors.title}</span>}
          </div>
          <div className='field'>
            <label>Author</label>
            <input
              type="text"
              name='author'
              value={formValues.author}
              onChange={handleChange}
            />
            {formErrors.author && <span className="error">{formErrors.author}</span>}
          </div>
          <div className='field'>
            <label>Publication Year</label>
            <input
              type="text"
              name='pbyear'
              value={formValues.pbyear}
              onChange={handleChange}
            />
            {formErrors.pbyear && <span className="error">{formErrors.pbyear}</span>}
          </div>
          <div className='field'>
            <label>Description</label>
            <input
              type="text"
              name='description'
              value={formValues.description}
              onChange={handleChange}
            />
            {formErrors.description && <span className="error">{formErrors.description}</span>}
          </div>
          <div className='field'>
            <label>Price</label>
            <input
              type="text"
              name='price'
              value={formValues.price}
              onChange={handleChange}
            />
            {formErrors.price && <span className="error">{formErrors.price}</span>}
          </div>
          <div className='field'>
            <label>Image URL</label>
            <input
              type='url'
              name='url'
              value={formValues.url}
              onChange={handleChange}
            />
            {formErrors.url && <span className="error">{formErrors.url}</span>}
          </div>
          <div className='field'>
            <label>Main Genre</label>
            <input
              type="text"
              name='main'
              value={formValues.genre.main}
              onChange={handleChange}
            />
            {formErrors.main && <span className="error">{formErrors.main}</span>}
          </div>
          <div className='field'>
            <label>Sub-Genre</label>
            <input
              type="text"
              name='sub'
              value={formValues.genre.sub}
              onChange={handleChange}
            />
            {formErrors.sub && <span className="error">{formErrors.sub}</span>}
          </div>
          <div className='field'>
            <label>Discount</label>
            <input
              type="text"
              name='discount'
              value={formValues.discount}
              onChange={handleChange}
            />
            {formErrors.discount && <span className="error">{formErrors.discount}</span>}
          </div>
          <div className='field'>
            <label>Language</label>
            <input
              type="text"
              name='language'
              value={formValues.language}
              onChange={handleChange}
            />
            {formErrors.language && <span className="error">{formErrors.language}</span>}
          </div>
          <div className='field'>
            <label>Quantity</label>
            <input
              type="text"
              name='quantity'
              value={formValues.quantity}
              onChange={handleChange}
            />
            {formErrors.quantity && <span className="error">{formErrors.quantity}</span>}
          </div>
          <div className='field'>
            <label>Publisher</label>
            <input
              type="text"
              name='publisher'
              value={formValues.publisher}
              onChange={handleChange}
            />
            {formErrors.publisher && <span className="error">{formErrors.publisher}</span>}
          </div>
          <div className='field'>
            <label>Rating</label>
            <input
              type="text"
              name='rating'
              value={formValues.rating}
              onChange={handleChange}
            />
            {formErrors.rating && <span className="error">{formErrors.rating}</span>}
          </div>
          <div className='btn'>
            <button type='submit' style={{ width: "100px", backgroundColor: "green", borderRadius: "8px", fontSize: "15px", color: "white" }}>Add Book</button>
          </div>
        </form>
        {registrationAttempted && (
          <div style={{ margin: "20px" }}>
            {isRegistered ? "Book Added Successfully" : `ERROR: ${errorMessage}`}
          </div>
        )}
      </div>
    </div>
  );
}

export default BookDetails;
