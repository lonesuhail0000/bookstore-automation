import React from 'react';
import { NavLink } from 'react-router-dom';
import './FeaturedAuthors.css';

const FeaturedAuthors = () => {
  const authors = [
    {
      id: "Arundhati Roy",
      name: 'Arundhati Roy',
      image: 'https://images.mubicdn.net/images/cast_member/57651/cache-229954-1497400964/image-w856.jpg',
      description: 'Arundhati Roy is an Indian writer, best known for her book The God of Small Things (1997), which won the Man Booker Prize for Fiction in 1997. She was born in Shillong, Meghalaya, India. Her parents were Rajib Roy, a Bengali Hindu tea plantation...',
      link:<NavLink to='/ARD' > Read More</NavLink>
    },
    {
      id: 2,
      name: 'Stephen Coonts',
      image: 'https://cdn.writerswrite.com/journal/jan00/stevecoonts.jpg',
      description: 'STEPHEN COONTS is the author of sixteen New York Times bestselling books that have been translated and published around the world. A former naval aviator and Vietnam combat veteran, he is a graduate of West Virginia University and the University of C...',
      link: <NavLink to='/coonts' > Read More</NavLink>
    },
    {
      id: 3,
      name: 'William Wordsworth',
      image: 'https://cdn.britannica.com/98/147598-050-F20C7EF3/engraving-William-Wordsworth-1833.jpg',
      description: 'On April 7, 1770, William Wordsworth was conceived in Cockermouth, Cumbria, England. Wordsworth\'s mom kicked the bucket when he was eight—this experience shapes a lot of his later work. Wordsworth went to Hawkshead Grammar School, where his adorat...',
      link: 'Read More'
    },
    {
      id: 4,
      name: 'Stephen King',
      image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR04EsdMcSy60-LRf2b8pRuhe4-qCSxzu591A&s',
      description: 'Stephen Edwin King was conceived in Portland, Maine in 1947, the second child of Donald and Nellie Ruth Pillsbury King. After his folks isolated when Stephen was a little child, he and his more established sibling, David, were raised by his mom. Part...',
      link: 'Read More'
    }
  ];

  return (
    <div className="featured-authors">
      <h2>Featured Authors</h2>
      <div className="authors-container">
        {authors.map((author, index) => (
          <div className="author-card" key={index}>
            <img src={author.image} alt={author.name} className="author-image" />
            <div className="author-info">
              <h3>{author.name}</h3>
              <p>{author.description}</p>
              {author.link}
              
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default FeaturedAuthors;
