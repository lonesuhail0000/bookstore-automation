import React, { useEffect, useState } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.css';
import { Navigation, Pagination } from 'swiper/modules';
import './TrendingBooks.css';
import axios from 'axios';

const TrendingBooks = () => {
  const [books, setBooks] = useState([]);

  useEffect(() => {
    axios.get('http://localhost:3201/items')
        .then(response => {
            const allBooks = response.data;
            const filteredBooks = allBooks.filter(book => book.title.split(' ').length < 3);
            setBooks(filteredBooks);
        })
        .catch(error => {
            console.error('There was an error fetching the items!', error);
        });
}, []);

  return (
    <div className="trending-books">
      <h2>Now Trending</h2>
      <Swiper
        slidesPerView={3}
        modules={[Navigation, Pagination]}
        navigation={true}
        pagination={{ clickable: true }}
        loop={true}
        speed={700}
        spaceBetween={0}
      >
        {books.map((book, index) => (

          book.rating > '4.7' &&  ( <SwiperSlide key={index}>
            <div className="book-card">
              <div className="discount-badge">{book.discount}%</div>
              <img src={book.url} alt={book.title} className="book-image" />
              <h3 className="book-title">{book.title}</h3>
              <p>{book.author}</p>
              <div className="rating">
                {'★'.repeat(Math.round(book.rating))}{'☆'.repeat(5 - Math.round(book.rating))}
                <span>{book.rating}</span>
              </div>
              <div className="price">
                <span className="final-price">₹{(book.price * (1 - book.discount / 100)).toFixed(2)}</span>
                <span className="original-price">₹{book.price}</span>
              </div>
            </div>
          </SwiperSlide>
        )))}
      </Swiper>
      <div className="see-all">SEE ALL</div>
    </div>
  );
};

export default TrendingBooks;
