import React, { useState } from 'react';
import './RequestBookForm.css';

const RequestBookForm = () => {
  const [email, setEmail] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  return (
    <div className="form-container">
      <h2>Request A Book</h2>
      <p>Please fill up the form below to request a book. We will inform you as soon as the book is available.</p>
      <form onSubmit={handleSubmit} className="book-form">
        <div className="form-group">
          <label htmlFor="isbn">ISBN13: <span>*</span></label>
          <input type="text" id="isbn" required />
        </div>
        <div className="form-group">
          <label htmlFor="title">Book Title: <span>*</span></label>
          <input type="text" id="title" required />
        </div>
        <div className="form-group">
          <label htmlFor="author">Author:</label>
          <input type="text" id="author" />
        </div>
        <div className="form-group">
          <label htmlFor="quantity">Quantity:</label>
          <input type="number" id="quantity" defaultValue="1" />
        </div>
        <div className="form-group">
          <label htmlFor="email">Email Id: <span>*</span></label>
          <input
            type="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <button type="button" className="verify-btn">Verify Email</button>
        </div>
        <div className="form-group">
          <label htmlFor="phone">Phone/Mobile:</label>
          <input type="tel" id="phone" />
        </div>
        <button type="submit" className="submit-btn">Submit</button>
      </form>
    </div>
  );
};

export default RequestBookForm;
