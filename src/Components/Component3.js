import React from 'react'
import {Swiper,SwiperSlide}  from 'swiper/react'
import "swiper/swiper-bundle.css";
import { Navigation, Pagination, } from 'swiper/modules';
import './component3.css'



const Component3 = () => {
  return (
    <div className='component3'>
        <hr style={{width:"90%"}}/>
        <Swiper
         
         slidesPerView={5}
         modules={[Navigation, Pagination,]}
         autoplay={true}
         navigation={true}
         loop={true}
         spaceBetween={"20px"}
         >
        <div className="swiper-02">
        <SwiperSlide className="swiperContent" >
            <img src="https://www.bookswagon.com/Images/staticimages/icon1.png" alt="" /> <p>Best Seller </p>
        </SwiperSlide>

        <SwiperSlide className="swiperContent" >
                <img src="https://www.bookswagon.com/Images/staticimages/icon8.png" alt="" /> <p>Award Winners </p>
        </SwiperSlide>
        <SwiperSlide className="swiperContent" >
                <img src="https://www.bookswagon.com/Images/staticimages/icon4.png" alt="" /> <p>Box Sets </p>
        </SwiperSlide>
        <SwiperSlide className="swiperContent" >
                <img src="https://www.bookswagon.com/Images/staticimages/icon7.png" alt="" /> <p>International Best Seller </p>
        </SwiperSlide>
        <SwiperSlide className="swiperContent" >
                <img src="https://www.bookswagon.com/Images/staticimages/icon2.png" alt="" /> <p>New Arrivals </p>
        </SwiperSlide>
        <SwiperSlide className="swiperContent" >
                <img src="https://www.bookswagon.com/Images/staticimages/icon5.png" alt="" /> <p>Fiction Books </p>
        </SwiperSlide>
        <SwiperSlide className="swiperContent" >
                <img src="https://www.bookswagon.com/Images/staticimages/tarot.png" alt="" /> <p>Tarot Cards </p>
        </SwiperSlide>
        <SwiperSlide className="swiperContent" >
                <img src="https://www.bookswagon.com/Images/staticimages/TodaysDeal.png" alt="" /> <p>Today's Deal </p>
        </SwiperSlide>
        </div>
        </Swiper>
        <hr style={{width:"95%"}}/>

    </div>
  )
}

export default Component3