import React from 'react';

function Footer() {
  return (
    <div className='Footers'>
        <div className="column1">
            <ul>
                <li style={{color:"red",fontWeight:"600"}}>Company</li>
                <li>About Us</li>
                <li>Career</li>
                <li>Blog</li>
                <li>Contact Us</li> {/* Corrected typo */}
            </ul>
        </div>
        <div className="column2">
            <ul>
                <li style={{color:"red",fontWeight:"600"}}>Policies</li>
                <li>Privacy Policy</li>
                <li>Terms of Use</li>
                <li>Secure Shopping</li>
                <li>Copyright Policy</li>
            </ul>
        </div>
        <div className="column3">
            <ul>
                <li style={{color:"red",fontWeight:"600"}}>Help</li>
                <li>Payment</li>
                <li>Shipping</li>
                <li>Return</li>
                <li>FAQ</li>
            </ul>
        </div>
        <div className="column4">
            <ul>
                <li style={{color:"red",fontWeight:"600"}}>Misc</li>
                <li>Affiliate</li> {/* Corrected typo */}
                <li>Site Map</li>
            </ul>
        </div>
    </div>
  );
}

export default Footer;
