import React, { useState } from 'react';
import { FaSearch } from "react-icons/fa";
import { FaRegCircleUser } from "react-icons/fa6";
import { IoIosArrowUp } from "react-icons/io";
import { CiShoppingCart } from "react-icons/ci";
import { IoMdArrowDropdownCircle } from "react-icons/io";
import { Link,NavLink } from 'react-router-dom';
import { Swiper, SwiperSlide } from 'swiper/react';
import "swiper/swiper-bundle.css";
import { Navigation, Pagination, Autoplay } from 'swiper/modules';


const Navbar = () => {
  const [showDropdown, setShowDropdown] = useState(false);

  const handleDropdownToggle = () => {
    setShowDropdown(!showDropdown);
  };

  return (
    <div className='navbar'>
      <div className='navContainer'>
        <div className='firstrow'>
          <div className='logo' >
            <Link to='/home' style={{textDecoration:"none",fontSize:"1.3rem"}}>The Eccentrc Store</Link> {/* Changed from Link to NavLink */}
          </div>
          <div className='searchbox'>
            <input type='text' placeholder='Title, Author, Publisher or ISBN' />
            <span><FaSearch /></span>
          </div>
          <div className='userDetails'>
            <div className='user'>
              <FaRegCircleUser />
              <span style={{ color: "white" }}>My Account</span>
              <IoIosArrowUp /> <hr />
              <CiShoppingCart />
              <div className='userhover'>
                <button style={{ backgroundColor: "red" }}>Login</button>
                <ul>
                  <li>Your Account</li>
                  <li>Personal Settings</li>
                  <li>Your Orders</li>
                  <li>Your Wishlist</li>
                  <li>Your Giftcard Certificate</li>
                  <li>Your Addresses</li>
                  <li>Change Password</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className='secondrow' style={{ color: "red" }}>
          <div className='nv2' style={{ color: "black", position: "relative" }} onClick={handleDropdownToggle}>
            <span>Books<IoMdArrowDropdownCircle /></span>
            {showDropdown && (
              <div className='userhover2' style={{ left: "0", position: "absolute", top: "100%", background: "white", zIndex: 10 }}>
                <ul>
                  <li>Fiction</li>
                  <li>Comedy</li>
                  <li>Best Seller</li>
                  <li>New Arrivals</li>
                  <li>Novels</li>
                  <li>Fantasy</li>
                </ul>
              </div>
            )}
          </div>
          <hr />
          <span><NavLink to='/newarrival' className='nv2' style={{ color: "black" }} activeClassName="activeLink"><p>New Arrivals</p></NavLink></span>
          <hr />
         <span><NavLink to='/bestseller' className='nv3' style={{ color: "black" }} activeClassName="activeLink"><p>Best Seller</p></NavLink> </span> 
          <hr />
          
          <span><NavLink to='/fiction' className='nv5' activeClassName="activeLink" style={{ color: "black" }} ><p>Fiction Books</p></NavLink></span>
          <hr />
          <span><NavLink to='/awardwinners' activeClassName="activeLink" className='nv6' style={{ color: "black" }}><p>Award Winners</p></NavLink> </span>
          <hr />
         <span><NavLink to='/featured' className='nv7' style={{ color: "black" }} activeClassName="activeLink" ><p>Featured Authors</p></NavLink> </span> 
          <hr />
          <span><NavLink to='/todaydeal' activeClassName="activeLink" className='nv8' style={{ color: "black" }}><p>Today's Deal</p></NavLink></span>
          <hr />
          <span><NavLink to='/requestbook' activeClassName="activeLink" className='nv9' style={{ color: "black" }}><p>Request a Book</p></NavLink></span>
        </div>
        <div className='image-container'>
          <img className='imagesSwiper' src='https://www.bookswagon.com/images/promotionimages/web/2_CuratedWeb1505.jpg' alt='imag 2' />
        </div>
        <div className='Swipper1'>
          <Swiper
            className='swiper1'
            slidesPerView={1}
            modules={[Navigation, Pagination, Autoplay]}
            autoplay={true}
            loop={true}
            pagination={{ clickable: true }}
            speed={700}
          >
            <SwiperSlide>
              <img className='imagesSwiper1' src={'https://www.bookswagon.com/bannerimages/89_inr.jpg'} alt='image3' />
            </SwiperSlide>
            <SwiperSlide>
              <img className='imagesSwiper1' src={'https://www.bookswagon.com/bannerimages/80_inr.jpg?v=5.2'} alt='image4' />
            </SwiperSlide>
            <SwiperSlide>
              <img className='imagesSwiper1' src={'https://www.bookswagon.com/bannerimages/79_inr.jpg?v=5.2'} alt='image4' />
            </SwiperSlide>
          </Swiper>
        </div>
      </div>
    </div>
  );
}

export default Navbar;