import React, { useEffect, useState } from 'react';
import './AuthorDetail.css';
import axios from 'axios';

const Coonts = () => {
  const [author, setAuthor] = useState([]);

  useEffect(() => {
    axios.get('http://localhost:3201/items')
      .then(response => {
        setAuthor(response.data);
      })
      .catch(error => {
        console.error('There was an error fetching the items!', error);
      });
  }, []);

  return (
    <div className="author-detail">
      <div className="author-header">
        <img src="https://cdn.writerswrite.com/journal/jan00/stevecoonts.jpg" alt="Stephen Coonts" className="author-detail-image" />
        <div>
          <h2>Stephen Coonts</h2>
          <p>
            Stephen Coonts is an American author, best known for his first novel "Flight of the Intruder" which became a best-seller and was adapted into a movie. Born on July 19, 1946, in Buckhannon, West Virginia, Coonts served as a naval aviator before becoming a full-time writer. His works often draw from his military experience and focus on aviation and thriller genres. Some of his popular books include "The Intruders," "The Minotaur," and "Under Siege."
          </p>
        </div>
      </div>
      <div className="books-list">
        {author && author.map((book, index) => (
          book.author === "Stephen Coonts" ? (
            <div key={index} className="book-card">
              <div className="book-info">
                <h3>{book.title}</h3>
                <img src={book.url} alt={book.title} className="book-thumbnail" />
                <p>By: Stephen Coonts</p>
                <p>Publisher: {book.publisher}</p>
                <div className="rating">
                  {'★'.repeat(Math.round(book.rating))}{'☆'.repeat(5 - Math.round(book.rating))}
                  <span>{book.rating}</span>
                </div>
                <div className="price">
                  <span className="discount">{book.discount}%</span>
                  <span className="original-price">₹{book.price}</span>
                  <span className="final-price">₹{(book.price * (1 - book.discount / 100)).toFixed(2)}</span>
                </div>
                <p>Binding: {book.binding}</p>
                <p>Release Date: {new Date(book.pbyear).toLocaleDateString()}</p>
                <p className="availability">{book.quantity > 0 ? 'Available' : 'Out of stock'}</p>
                <p className="shipping">Ships within 4-6 Days</p>
                <div className="buttons">
                  <button className="add-to-cart">Add to cart</button>
                  <button className="add-to-wishlist">Add to Wishlist</button>
                </div>
              </div>
            </div>
          ) : null
        ))}
      </div>
    </div>
  );
};

export default Coonts;
