import React, { useEffect, useState } from 'react';
import './AuthorDetail.css';
import axios from 'axios';

const Arundhati = () => {
  const [author, setAuthor] = useState([]);

  useEffect(() => {
    axios.get('http://localhost:3201/items')
        .then(response => {
            setAuthor(response.data);
        })
        .catch(error => {
            console.error('There was an error fetching the items!', error);
        });
}, []);

  return (
    <div className="author-detail">
      <div className="author-header">
        <img src="https://images.mubicdn.net/images/cast_member/57651/cache-229954-1497400964/image-w856.jpg" alt="Arundhati Roy" className="author-detail-image" />
        <div>
          <h2>Arundhati Roy</h2>
          <p>
Arundhati Roy is an Indian author and activist, renowned for her debut novel "The God of Small Things," which won the Man Booker Prize in 1997. Born on November 24, 1961, in Shillong, India, she studied architecture but gained international fame through her literary work. Besides fiction, Roy is a prominent political activist, known for her outspoken views on social justice, environmental issues, and human rights. Her essays and non-fiction works, such as "The End of Imagination" and "Field Notes on Democracy," reflect her commitment to activism and critique of global and Indian political landscapes</p>
        </div>

        
      </div>
      <div className="books-list">
        {author && author.map((book, index) => (
          book.author === "Arundhati Roy" ? (
            <div key={index} className="book-card">
              <div className="book-info">
                <h3>{book.title}</h3>
                <img src={book.url} alt={book.title} className="book-thumbnail" />
                <p>By: Arundhati Roy</p>
                <p>Publisher: {book.publisher}</p>
                <div className="rating">
                  {'★'.repeat(Math.round(book.rating))}{'☆'.repeat(5 - Math.round(book.rating))}
                  <span>{book.rating}</span>
                </div>
                <div className="price">
                  <span className="discount">{book.discount}%</span>
                  <span className="original-price">₹{book.price}</span>
                  <span className="final-price">₹{(book.price * (1 - book.discount / 100)).toFixed(2)}</span>
                </div>
                <p>Binding: {book.binding}</p>
                <p>Release Date: {new Date(book.pbyear).toLocaleDateString()}</p>
                <p className="availability">{book.quantity > 0 ? 'Available' : 'Out of stock'}</p>
                <p className="shipping">Ships within 4-6 Days</p>
                <div className="buttons">
                  <button className="add-to-cart">Add to cart</button>
                  <button className="add-to-wishlist">Add to Wishlist</button>
                </div>
              </div>
            </div>
          ) : null
        ))}
      </div>
    </div>
  );
};

export default Arundhati;
