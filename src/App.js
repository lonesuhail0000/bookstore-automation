import './App.css';
import { Swiper, SwiperSlide } from 'swiper/react';
import "swiper/swiper-bundle.css";
import { Navigation, Pagination, Autoplay } from 'swiper/modules';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './Pages/Home';
import Navbar1 from './Components/Navbar';
import Footer from './Components/Footer';
import Component3 from './Components/Component3';
import ItemList from './Pages/ItemList';
import BestSeller from './Pages/BestSeller';
import Fiction from './Pages/Fiction';
import AwardWinners from './Pages/AwardWinners';
import FeaturedAuthors from './Pages/Featured';
import AuthorDetail from './Pages/AuthorDetails';
import Arundhati from './Authors/Adr';
import Coonts from './Authors/Coonts';
import RequestBookForm from './Components/RequestBookForm';

function App() {
  return (
    <div id="root">
      <div className='offer'>
        <Swiper
          className='swiper'
          slidesPerView={1}
          modules={[Navigation, Pagination, Autoplay]}
          autoplay={true}
          loop={true}
          style={{margin:"0"}}
          speed={700}
        >
          <SwiperSlide className='swiper-01'>Latest Releases Collection</SwiperSlide>
          <SwiperSlide className='swiper-01'>Collect Father's Day Gift Now!</SwiperSlide>
          <SwiperSlide className='swiper-01'>#1 Best Seller 2024 "The Women" By "Kristin Hannah"</SwiperSlide>
        </Swiper>
      </div>
      <div className="content">
        <BrowserRouter>
          <Navbar1 />
          <Routes>
            <Route path='/home' element={<Home />} />
            <Route path='/newarrival' element={<ItemList />} />
            <Route path='/bestseller' element={<BestSeller />} />
            <Route path='/fiction' element={<Fiction />} />
            <Route path='/awardwinners' element={<AwardWinners />} />
            <Route path='/todaydeal' element={<AwardWinners />} />
            <Route path='/featured' element={<FeaturedAuthors />} />
            <Route path="/author/:authorId" element={<AuthorDetail />}/>
            <Route path="/requestbook" element={<RequestBookForm />}/>
            <Route path="/ARD" element={<Arundhati/>}/>
            <Route path="/coonts" element={<Coonts/>}/>
            <Route path='/*' element={<Home />} /> {/* Default route */}
          </Routes>
        </BrowserRouter>
      </div>
      <Component3 />
      <Footer />
    </div>
  );
}

export default App;
